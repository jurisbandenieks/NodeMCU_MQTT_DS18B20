#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS D1
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

const char* ssid = "NetowrkName";
const char* password =  "NetworkPassword";
const char* mqttServer = "m23.cloudmqtt.com";
const int mqttPort = 00000;
const char* mqttUser = "User";
const char* mqttPassword = "Password";
 
WiFiClient espClient;
PubSubClient client(espClient);
float t; // temperature

void setup() {
 
  Serial.begin(115200);
  sensors.begin();
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("NodeMCUCLient", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
}
 
void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");
 
}
 
void loop() {
  sensors.requestTemperatures();
  t = sensors.getTempCByIndex(0);
  String temperature = String(t);

  // Just debug messages
  Serial.print( "Sending temperature: [" );
  Serial.print( temperature );
  Serial.print( "]   -> " );

  // Prepare a JSON payload string
  String payload = "{";
  payload += "\"temperature\":"; payload += temperature;
  payload += "}";

  // Send payload
  char attributes[100];
  payload.toCharArray( attributes, 100 );
  client.publish( "NodeMCUClient", attributes );
  Serial.println( attributes );
  client.loop();
  delay(6000);
}
